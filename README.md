Ansible Repo for the CCCWI Systems
==================================

Getting Started:
================

To install the required roles just run:

```ansible-playbook -l localhost -i production site.yml```

This will ensure that all roles secified in requirements.yml are installed. Be aware that in this way you could p0wn yourself if the requirements.yml comes from a untrusted source, or includes untrusted roles.
